var apps;
var selectedAppName;
var selectedAppID = "#app0";
var webswingID = getCookie("webswingID");
var dirId = null;
var refresh = {"i": 2, "j": 1, "c": 2.5};

if (webswingID) {
    $.getJSON("/dir?webswingID=" + webswingID, function (data) {
        if (data) {
            var i = 0;
            for (var app in data) {
                $("#apps").append('<a id="app' + i + '" class="list-group-item" onclick="selectApp(' + i + ',\'' + app + '\')">' + app + '</a>');
                i++;
            }
            apps = data;
            if(Object.keys(apps).length !== 0) {
                selectApp(0, Object.keys(data)[0]);
            } else {
                showErrorDialog("Please start an Application.");
            }
        } else {
            $("#dirs").html('<div class="dir col-md-12"> empty '); //TODO error Nachricht war leer,weil es kein user.home/usrName Verz gibt
        }
    }).error(function () {
        showErrorDialog("Please Login.");
    });
}

function getUrlParam(name) {
    var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
    if (results === null) {
        return null;
    }
    else {
        return results[1] || 0;
    }
}

function selectApp(i, appName) {
    var root = apps[appName];
    selectedAppName = appName;
    root.parentID = null;
    dirId = [root.parentID, root.id];
    $("#path").html('');
    if (root.files)
        updateUI(root);
    else {
        //empty
        $("#dirs").html('<div class="dir col-md-12"><span class="glyphicon glyphicon-folder-open"></span>&nbsp;&nbsp;..');
        $("#files").html('');
    }    //
    var id = "#app" + i;
    $(selectedAppID).removeClass("active");
    $(id).addClass("active");
    selectedAppID = id;
}

function updatePath(tree) {
    __updatePath(tree.id);
    $("#path").append('<a id="' + tree.id + '" onclick="updatePathAndUpdateUI(' + tree.id + ')">/' + tree.name);
}

function updatePathAndUpdateUI(id) {
    __updatePath(id);
    s(id);
}

function __updatePath(id) {
    var tmp = "";
    var ok = true;
    $("#path > a").each(function (index) {
        if (this.id == id) {
            ok = false;
        }
        if (ok) {
            tmp += this.outerHTML;
        }
    });
    $("#path").html(tmp);
}

function updateUI(tree) {
    updatePath(tree);
    $("#dirs").html('');
    $("#files").html('');
    var d = '<span class="glyphicon glyphicon-folder-open"></span>&nbsp;&nbsp;';
    var f = '<span class="glyphicon glyphicon-file"></span>&nbsp;&nbsp;';
    if (tree.parentID !== null) {
        $("#dirs").append('<div class="dir col-md-12" onclick="s(' + tree.parentID + ')">' + d + '.. ');
    }
    var app = encodeURI(selectedAppName);
    for (var i in tree.files) {
        if (tree.files[i].size === undefined) {
            $("#dirs").append('<div class="dir col-md-12" onclick="s(' + tree.files[i].id + ')">' + d + tree.files[i].name);
        } else {
            $("#files").append('<div class="file col-md-7" "><a href="/dir?app=' + app + '&fileId=' + tree.files[i].id + '">' + f + tree.files[i].name + '</a></div>');
            $("#files").append('<div class="file col-md-2">' + formatFileSize(tree.files[i].size));
            $("#files").append('<div class="file col-md-3">' + new Date(tree.files[i].lastModified).toUTCString());
        }
    }
}

function s(id) {
    var node = search(apps[selectedAppName], id, null);
    updateUI(node);
    dirId = [node.parentID, node.id];
}
function search(node, id, parentID) {
    if (node !== null) {
        if (id === null) {
            return node;
        } else if (node.id === id) {
            node.parentID = parentID;
            return node;
        } else {
            for (var i in node.files) {
                //console.log(i + " " + node.files[i] + " " + node.name + " " + node.id); 
                if (node.files[i].size === undefined) {
                    var n = search(node.files[i], id, node.id);
                    if (n !== null) {
                        return n;
                    }
                }
            }
        }
    }
    return null;
}

function reload() {
    var app = encodeURI(selectedAppName);
    var dpd = $("#displayPublicDir").is(":checked"); 
    $("#loading").show();
    $.getJSON("/dir?app=" + app + "&dirId=" + dirId[1] + "&dpd=" + dpd, function (data) {
        data.parentID = dirId[0];
        updateUI(data);
        $("#loading").hide(1000);
        var node = search(apps[selectedAppName], dirId[1], null);
        node.files = data.files;
    }).error(function (f) {
        showErrorDialog("Your session has expired, please login again.");
    });
}

function showErrorDialog(errorMsg){
    $("#autoRefresh").prop("checked", false);
    $("#main").hide();
    $("#errorDialog").show();
    $("#errorDialog").text(errorMsg); 
    setTimeout(function(){ window.location.replace("/"); }, 5000);
}

function __autoRefresh() {
    var time = (refresh.c / 2 * (refresh.i + 1)) * 1000;
    if (refresh.j < refresh.i) {
        refresh.j++;
    } else {
        refresh.i++;
        refresh.j = 0;
    }
    
    if(refresh.i > 12) {
        $("#autoRefresh").prop("checked", false);
    }
    if ($("#autoRefresh").is(":checked")) {
        reload();
        setTimeout(__autoRefresh, time);
    }
}

function autoRefresh() {
    if ($("#autoRefresh").is(":checked")) {
        __autoRefresh();
    } else {
        refresh.i = 2;
        refresh.j = 0;
    }
}

function displayPublicDir() {
    reload();
}

function formatFileSize(bytes) {
    if (typeof bytes !== 'number') {
        return '';
    }
    if (bytes >= 1000000000) {
        return (bytes / 1000000000).toFixed(2) + ' GB';
    }
    if (bytes >= 1000000) {
        return (bytes / 1000000).toFixed(2) + ' MB';
    }
    return (bytes / 1000).toFixed(2) + ' KB';
}

function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ')
            c = c.substring(1);
        if (c.indexOf(name) == 0)
            return c.substring(name.length, c.length);
    }
    return "";
}