package org.webswing.server.handler;

import java.io.IOException;
import java.util.Enumeration;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.codec.binary.Base64;
import org.apache.shiro.SecurityUtils;
import org.webswing.model.internal.SystemProperty;
import org.webswing.model.server.SwingApplicationDescriptor;
import org.webswing.server.ConfigurationManager;
import org.webswing.server.SwingInstance;
import org.webswing.server.SwingInstanceManager;

public class ParameterServlet extends HttpServlet {

    private static final long serialVersionUID = 7510963790722200536L;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String userId = (String) SecurityUtils.getSubject().getPrincipal();

        if (userId == null) {
            resp.sendRedirect("/messagebox.html?type=4");
            return;
        }

        String webswingId = webswingIDfromCoockie(req);
        String appName = null;
        Enumeration<String> parameterNames = req.getParameterNames();
        String param = "";
        while (parameterNames.hasMoreElements()) {
            String paramName = parameterNames.nextElement();
            String[] paramValues = req.getParameterValues(paramName);
            for (int i = 0; i < paramValues.length; i++) {
                String paramValue = paramValues[i];
                if (paramName.equals("app")) {
                    appName = paramValue;
                } else {
                    param += paramName + ";" + paramValue + ";";
                }
            }
        }

        if (appName == null || param.isEmpty() || webswingId == null) {
            resp.sendRedirect("/messagebox.html?type=3");
        } else {
            SwingApplicationDescriptor sad = ConfigurationManager.getInstance().getApplication(appName);
            if (sad == null) {
                // app not exist
                resp.sendRedirect("/messagebox.html?type=1&app=" + appName);
            } else {
                String clientId = searchRunningApp(userId, webswingId, appName);
                if (clientId != null) {
                    sendParameterToSwing(clientId, param);
                    resp.sendRedirect("/messagebox.html?type=0&app=" + appName);
                } else {
                    //app not running
                    String link = getUrlToStartApp(resp, param, appName);
                    resp.sendRedirect("/messagebox.html?type=2&app=" + appName + "&link=" + link);
                }
            }

        }
    }

    private String webswingIDfromCoockie(HttpServletRequest req) {
        Cookie[] arrayOfCookie = req.getCookies();
        String webswingID = null;
        for (int i = 0; i < arrayOfCookie.length; i++) {
            Cookie c = arrayOfCookie[i];
            if (c.getName().equals("webswingID")) {
                webswingID = c.getValue();
                break;
            }
        }
        return webswingID;
    }

    private String getUrlToStartApp(HttpServletResponse resp, String param, String appName) throws IOException {
        String replace = "";
        String[] array = param.split(";");
        for (int i = 0; i < array.length; i += 2) {
            replace += array[i] + "=" + array[i + 1] + "&";
        }
        String link = "/?app=" + appName + "&args=" + replace;
        byte[] base = Base64.encodeBase64(link.getBytes());

        return new String(base);
    }

    public void startSwingInstance(HttpServletResponse resp, String param, String appName) throws IOException {
        resp.sendRedirect(getUrlToStartApp(resp, param, appName));
    }

    private String searchRunningApp(String userId, String webswingId, String app) {
        SwingInstanceManager sw = SwingInstanceManager.getInstance();
        String clientID = userId + webswingId + app;
        for (SwingInstance instance : sw.getSwingInstanceSet()) {
            if (instance.getClientId().equals(clientID)) {
                return clientID;
            }
        }
        return null;
    }

    private void sendParameterToSwing(String clientID, String param) {
        SystemProperty json = new SystemProperty();
        json.setClientId(clientID);
        json.setKey("swingparam");
        json.setValue(param);
        SwingInstanceManager.getInstance().sendMessageToSwing(null, clientID, json);
    }

}
