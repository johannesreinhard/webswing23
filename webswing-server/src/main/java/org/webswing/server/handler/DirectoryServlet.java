package org.webswing.server.handler;

 
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.Closeable;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.shiro.SecurityUtils;
import org.webswing.Constants;
import org.webswing.model.internal.SystemProperty;
import org.webswing.model.server.SwingApplicationDescriptor;
import org.webswing.model.server.WebswingConfiguration;
import org.webswing.server.ConfigurationManager;
import org.webswing.server.SwingInstance;
import org.webswing.server.SwingInstanceManager;
import org.webswing.server.util.ServerUtil;

public class DirectoryServlet extends HttpServlet {


    private HashMap<String, AppData> appMap;
     
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String userId = (String) SecurityUtils.getSubject().getPrincipal();
        String fileId = req.getParameter("fileId");
        String appId = req.getParameter("app");
        String dirId = req.getParameter("dirId");
        String displayPublicDir = req.getParameter("dpd");
        
        
        
        
       // String u = req.getParameter("user");
        String webswingID = req.getParameter("webswingID");
        
        
        System.out.println("session id " + req.getSession().getId());
        System.out.println("session " + req.getSession().getAttribute("heinrich"));
   //     System.out.println("u " + u);
        System.out.println("webswingID     " + webswingID);
         
        if (userId == null) {
            resp.sendError(HttpServletResponse.SC_FORBIDDEN); // 403.
            return;
        }
        
        if (fileId == null && dirId == null || appMap == null) { 
            HashMap<String, String> appAndUserMap = updateUserNameInSwingInstance(userId, webswingID);
            sendFileDirectoryAsJson(resp, userId, appAndUserMap);
        } else if (dirId != null && appMap != null) { 
            sendUpdatFileDirectoryAsJson(resp, appId, dirId, displayPublicDir);
        } else {            
            sendFile(resp, appId, fileId);
        }
    }

    private void close(Closeable resource) {
        if (resource != null) {
            try {
                resource.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public Verzeichnis init(String appName, File folder, String usernameInSwing) {
        Verzeichnis wurzelVerz = new Verzeichnis(folder.getName(), folder.getAbsolutePath());
        getFileTree(folder, wurzelVerz);
        appMap.put(appName, new AppData(wurzelVerz, usernameInSwing));
        return wurzelVerz;
    }
    
    public void addPublicFolder(Verzeichnis wurzelVerz, File folder, String name) { 
        Verzeichnis pubWurzelVerz = new Verzeichnis(folder.getName() + "_" + name, folder.getAbsolutePath());
        getFileTree(folder, pubWurzelVerz);
        wurzelVerz.add(pubWurzelVerz);
    }
    
    
    public void addPublicFolders(File rootFolder, File userFolder, Verzeichnis rootVerz) {
        try {
            for (File file : rootFolder.listFiles()) {
                if (file.isDirectory() && !file.getCanonicalPath().equals(userFolder.getCanonicalPath())) {
                    File publicFolder = new File(file, "public");
                    addPublicFolder(rootVerz, publicFolder, file.getName());
                }
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
    

    private void getFileTree(File folder, Verzeichnis root) {
        if (folder.exists()) {
            for (File fileEntry : folder.listFiles()) {
                if (fileEntry.isDirectory()) {
                    Verzeichnis v = new Verzeichnis(fileEntry.getName(), fileEntry.getAbsolutePath());
                    if (root != null) {
                        root.add(v);
                    }
                    getFileTree(fileEntry, v);
                } else {
                    root.add(new Datei(fileEntry.getName(), fileEntry.getAbsolutePath(), fileEntry.length(), fileEntry.lastModified()));
                }
            }
        }
    }
    
    public Verzeichnis search(Verzeichnis node, long id) {
        if (node != null) {
            if (node.getId() == id) {
                return node;
            } else {
                for (Verzeichnis v : node.getDateiListe()) {
                    if (v instanceof Verzeichnis) {
                        Verzeichnis tmp = search(v, id);
                        if (tmp != null) {
                            return tmp;
                        }
                    }
                }
            }
        }
        return null;
    }
    
    
    public void loeschen_initappMap(){ /* TODO */
        appMap = new HashMap<String, AppData>();
    }
   
    private HashMap<String, String> updateUserNameInSwingInstance(String userId, String webswingId) {
        SwingInstanceManager sw = SwingInstanceManager.getInstance();
        WaitForResponseThread threads[] = new WaitForResponseThread[sw.getSwingInstanceSet().size()];       
        int i = 0;
        HashMap<String, String> map = new HashMap<String, String>(); // appName, Username
        SystemProperty json = new SystemProperty();
        json.setKey("webswingUsername");  
        for (SwingInstance instance : sw.getSwingInstanceSet()) {
            if(instance.getClientId().startsWith(userId + webswingId)) {
                if(instance.useApplicationDefinedRootDir()) {
                    json.setClientId(instance.getClientId());
                    SwingInstanceManager.getInstance().sendMessageToSwing(null, instance.getClientId(), json);
                    System.out.println("session id instance " + instance.getSessionId());
                    threads[i] = new WaitForResponseThread(instance);
                    threads[i].start();
                    i++;
                } else {
                    map.put(instance.getApplicationName(), "");
                }
            }
        }
        try {
            // Wait for all of the threads to finish.
            for (Thread thread : threads) {
                if(thread != null){
                    thread.join();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
 
         
        String username;
        //add app with useApplicationDefinedRootDir = true
        for (int j = 0; j < i; j++) {
            username = threads[j].getInstance().getUserNameInSwingInstance();
            if(username != null) {
                map.put(threads[j].getInstance().getApplicationName(), username);
            }
        }
        return map;
    }

    private void sendFileDirectoryAsJson(HttpServletResponse resp, String userId,  HashMap<String, String> appAndUserMap) throws IOException {
        appMap = new HashMap<String, AppData>(); 
        WebswingConfiguration wc = ConfigurationManager.getInstance().getLiveConfiguration();
        String json = "{ ";
        File d;
        File rootFolder;
        for (SwingApplicationDescriptor appDesc : wc.getApplications()) {
            String usernameInSwing = appAndUserMap.get(appDesc.getName());
            if(usernameInSwing != null) {
                if (ServerUtil.isUserAuthorizedForApplication(userId, appDesc)) {
                    rootFolder = new File(appDesc.getHomeDir(), Constants.FILEUPLOAD_DIR );
                    d = new File(rootFolder,usernameInSwing);
                    if (d.exists()) {
                        Verzeichnis v = init(appDesc.getName(), d, usernameInSwing);
                        if(usernameInSwing.length() > 0) {
                            addPublicFolders(rootFolder, d, v);
                        }
                        json += "\"" + appDesc.getName() + "\": " + v.toJson() + ",";
                    }
                }
            }
        }
        json = json.substring(0, json.length() - 1);
        json += "}";
        resp.getWriter().write(json);
    }

    private void sendFile(HttpServletResponse resp, String appId, String fileId) throws IOException {
        final int DEFAULT_BUFFER_SIZE = 10240; // 10KB.
        Verzeichnis rootDir = appMap.get(appId).getDirNode();
        Verzeichnis v = search(rootDir, Long.valueOf(fileId));
        if (v == null) {
            //TODO error (not found) 404 
            resp.sendError(HttpServletResponse.SC_NOT_FOUND); // 404.
            return;
        }
        File file = new File(v.getAbsolutePath());
        resp.reset();
        resp.setBufferSize(DEFAULT_BUFFER_SIZE);
        resp.setContentType("application/octet-stream");
        resp.setHeader("Content-Length", String.valueOf(file.length()));
        resp.setHeader("Content-Disposition", "attachment; filename=\"" + file.getName() + "\"");
        BufferedInputStream input = null;
        BufferedOutputStream output = null;
        try {
            input = new BufferedInputStream(new FileInputStream(file), DEFAULT_BUFFER_SIZE);
            output = new BufferedOutputStream(resp.getOutputStream(), DEFAULT_BUFFER_SIZE);
            byte[] buffer = new byte[DEFAULT_BUFFER_SIZE];
            int length;
            while ((length = input.read(buffer)) > 0) {
                output.write(buffer, 0, length);
            }
        } finally {
            close(output);
            close(input);
        }
    }
    
    public void sendUpdatFileDirectoryAsJson(HttpServletResponse resp, String appId, String directoryID, String displayPublicDir) throws IOException {
        Verzeichnis rootDir = appMap.get(appId).getDirNode();  
        Verzeichnis v = search(rootDir, Long.valueOf(directoryID)); 
        if (v == null) {
            resp.sendError(HttpServletResponse.SC_NOT_FOUND); // 404.
            return;
        }
        v.resetList();
        String usernameInSwing = appMap.get(appId).getUserName(); 
        System.out.println("== " + new File(v.getAbsolutePath()) + " \n\t " + v.getAbsolutePath());            
        getFileTree(new File(v.getAbsolutePath()), v);
        if(v.getId() == rootDir.getId()){ // is root  TODO: directoryID
            if(usernameInSwing.length() > 0 && "true".equals(displayPublicDir)) {
                File d = new File(rootDir.getAbsolutePath());
                addPublicFolders(d.getParentFile(), d, v);
            }
        }
        resp.getWriter().write(v.toJson());
    }
}

class AppData {
    private Verzeichnis root;
    private String userName;

    public AppData(Verzeichnis root, String userName) {
        this.root = root;
        this.userName = userName;
    }

    public Verzeichnis getDirNode() {
        return root;
    }

    public void setRoot(Verzeichnis root) {
        this.root = root;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }
}

class Verzeichnis {
    private ArrayList<Verzeichnis> dateiListe;
    private int id;
    private String absolutePath;
    private String name;

    public Verzeichnis(String name, String absolutePath) {
        this.dateiListe = new ArrayList<Verzeichnis>();
        this.name = name;
        this.absolutePath = absolutePath;
        id = absolutePath.hashCode(); // TODO vllt anders
    }

    public ArrayList<Verzeichnis> getDateiListe() {
        return dateiListe;
    }
    
    public void resetList(){
        dateiListe = null;
        dateiListe = new ArrayList<Verzeichnis>();
    }

    public void add(Verzeichnis d) {
        dateiListe.add(d);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAbsolutePath() {
        return absolutePath;
    }

    public void setAbsolutePath(String absolutePath) {
        this.absolutePath = absolutePath;
    }

    public int getId() {
        return id;
    }

    @Override
    public String toString() {
        return absolutePath;
    }

    public String toJson() {
        String json = "{";
        if (!dateiListe.isEmpty()) {
            json += "\"files\": [";
            for (Verzeichnis v : dateiListe) {
                json += v.toJson();
                json += ",";
            }
            json = json.substring(0, json.length() - 1);
            json += "], ";
        }
        json += "\"id\": " + id + ",";
        json += "\"name\": \"" + name + "\" }";
        return json;
    }
}

class Datei extends Verzeichnis {

    private long size;
    private long lastModified;
    
    public Datei(String name, String absolutePath, long size, long lastModified) {
        super(name, absolutePath);
        this.size = size;
        this.lastModified =  lastModified;
    }

    public long getSize() {
        return size;
    }

    public long getLastModified() {
        return lastModified;
    }
    
    public void setSize(long size) {
        this.size = size;
    }

    @Override
    public String toJson() {
        String json = super.toJson();
        json = json.substring(0, json.length() - 2);
        json += ", \"size\": " + size;
        json += ", \"lastModified\": " + lastModified + "}";
        return json;
    }
}


class WaitForResponseThread extends Thread{
    
    private SwingInstance instance; 
    
    public WaitForResponseThread (SwingInstance i) {
        this.instance = i;
        instance.setWaitingForResponse(true);
    }

    public SwingInstance getInstance() {
        return instance;
    }
    
    public void run(){
        while(instance.isWaitingForResponse()) {
            try {
                Thread.sleep(400);
            } catch (InterruptedException ex) {
            }
        }
        System.out.println(instance.getUserNameInSwingInstance() + " <- antwort "); 
    }
}
