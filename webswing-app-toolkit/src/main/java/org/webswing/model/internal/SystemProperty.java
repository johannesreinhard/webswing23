package org.webswing.model.internal;


import org.webswing.model.MsgIn;
import org.webswing.model.MsgInternal;

public class SystemProperty implements MsgInternal, MsgIn {
    private String key;
    private String value;
    private String clientId;

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getKey() {
        return key;
    }

    public String getValue() {
        return value;
    }
    
    
}
